set nocompatible
" Setting up Vundle - the vim plugin bundler
    let iCanHazVundle=1
    let vundle_readme=expand('~/.vim/bundle/vundle/README.md')
    if !filereadable(vundle_readme)
        echo "Installing Vundle.."
        echo ""
        silent !mkdir -p ~/.vim/bundle
        silent !git clone https://github.com/gmarik/vundle ~/.vim/bundle/vundle
        let iCanHazVundle=0
    endif
    set rtp+=~/.vim/bundle/vundle/
    call vundle#rc()
    "Add your bundles here
    Bundle 'tpope/vim-fugitive'
    Bundle 'kien/ctrlp.vim'
    Bundle 'tpope/vim-surround'
    Bundle 'vim-scripts/JSON.vim'
    Bundle 'scrooloose/nerdtree.git'
    Bundle 'scrooloose/syntastic.git'
	Bundle 'tell-k/vim-autopep8.git'
	Bundle 'mileszs/ack.vim'
	Bundle 'davidhalter/jedi-vim'
    if iCanHazVundle == 0
        echo "Installing Bundles, please ignore key map error messages"
        echo ""
        :BundleInstall
    endif
" Setting up Vundle - the vim plugin bundler end
"
filetype plugin indent on
syntax on

let loaded_matchparen = 1
"let g:ctrlp_clear_cache_on_exit = 1
let g:ctrlp_cache_dir = $HOME.'/.cache/ctrlp'
let g:ctrlp_clear_cache_on_exit=0
let go_highlight_trailing_whitespace_error=0
let mapleader = ","
let g:autopep8_max_line_length=79
let g:autopep8_aggressive=2
let g:autopep8_disable_show_diff=1
let NERDTreeShowHidden=1

set lsp=5
set nocursorline
"set background=dark
set clipboard=unnamed
set hidden
set autoindent
set smartindent
set gdefault
set smarttab
set ignorecase
set smartcase
set incsearch
set hlsearch
set history=2000
set undolevels=2000
set title
set vb t_vb=
set showmatch
set noerrorbells
set nuw=1
set backspace=indent,eol,start
set shiftwidth=4
set tabstop=4
set noexpandtab
set scrolloff=2
set noswapfile
set nowb
set nobackup
set viminfo^=%
set linespace=0
set shortmess=aOstT
set nu
set wildignore=*/.git/*,*/templates_c/*,*/live/*
set shortmess+=I
set splitbelow
set splitright
set encoding=utf-8
set wildmenu
set wildmode=list:longest,full

cmap w!! w !sudo tee % >/dev/null

inoremap jj <Esc>

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
nnoremap <Leader>g :GundoToggle<CR>
nnoremap <CR> o<Esc>k
nnoremap <Space>j <C-d>
nnoremap <Space>k <C-u>
nnoremap ; :
nnoremap B ^
nnoremap E $
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
nnoremap / /\v
nnoremap <Leader><CR> i<CR><Esc>
nnoremap <silent> <Leader>a :nohl<CR>
nnoremap <Leader>n :NERDTreeToggle<CR>
nnoremap <Leader>y "+y
nnoremap <Leader>/ :Ack
nnoremap <Leader>q :q!<CR>

autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 expandtab

hi Directory guifg=#FF0000 ctermfg=red
